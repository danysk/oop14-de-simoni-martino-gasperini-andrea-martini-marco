/**
 * 
 */
package esseri;

import javax.swing.JPanel;

import controller.AbstractInsertionPanelController;

/**
 * @author Andrea
 * 
 *         Lattuga ghiaccio � una consumabile , pi� precisamente non fa nulla
 *         quando viene giocata, infatti in faiRobe se non � attaccata ho
 *         implementato nessunAzione, ma nel momento in cui viene attaccata ,
 *         crea un piccolo danno allo zombie
 *
 */
public class LattugaGhiaccio extends Consumabili {

	private static String img_filePath = "lattugaghiaccio.jpe";

	public LattugaGhiaccio() {

		super(img_filePath, Utility.PIANTA_VITA_ALTA, 0.0,
				Utility.PIANTA_TEMPO_BASSO, TipoTerreno.CORTILE);

	}

	public void inviaAzione(
			int tempo_trascorso,
			AbstractInsertionPanelController<Azione, ? extends JPanel> controller,
			Posizione2D posizione) {

		if (canAct(tempo_trascorso)) {
			if (attaccato && !hoattaccato) {
				controller.insert(new Attacco(TipoEssere.ZOMBIE,
						new Posizione2D(0, 0), danno, NessunAzione
								.getInstance(), posizione));

				hoattaccato = true;
			}

			else if (attaccato && hoattaccato == true) {
				controller.insert(new Attacco(TipoEssere.ZOMBIE,
						new Posizione2D(0, 0), this.life, NessunAzione
								.getInstance(), posizione));
			}
		}
	}

	@Override
	public int getSoliRichiesti() {

		return 25;
	}

	@Override
	public TipoTerreno getTerreno() {

		return TipoTerreno.CORTILE;
	}

	@Override
	public void eseguireAllaMorte() {

	}

	@Override
	public boolean isDead() {
		return this.life <= 0;
	}

}
