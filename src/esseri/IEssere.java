/**
 * 
 */
package esseri;

import java.awt.image.BufferedImage;

import javax.swing.JPanel;

import controller.AbstractInsertionPanelController;

/**
 * Interfaccia dell'astratta Essere
 * 
 * @author Martino
 * 
 */
public interface IEssere {
	
	public BufferedImage getImg();
	public TipoEssere getTipoEssere();
	public TipoTerreno getTerreno();
	public void prendiDanno(double d);
	public void eseguireAllaMorte(); //principalmente, effettua l'animazione
	public int getTempoRichiesto();
	public int getTempoTrascorso();
	public boolean isDead();
	
	public void inviaAzione(int tempo_trascorso,AbstractInsertionPanelController<Azione, ? extends JPanel > controller, Posizione2D pos);

}


