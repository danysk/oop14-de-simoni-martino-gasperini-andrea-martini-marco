/**
 * 
 */
package esseri;

import javax.swing.JPanel;

import controller.AbstractInsertionPanelController;

/**
 * @author Andrea
 *
 *         Sostanzialmente questa classe Girasole non fa nulla (non attacca )
 *         per questo nel metodo fai robe ho implementato
 *         controller.insert(nessunAzione)
 */
public class Girasole extends Support {

	private static String img_filePath = "girasole.jpe";

	public Girasole() {

		super(img_filePath, Utility.PIANTA_VITA_MEDIA, 0.0,
				Utility.PIANTA_TEMPO_BASSO, TipoTerreno.CORTILE);

	}

	public void inviaAzione(
			int tempo_trascorso,
			AbstractInsertionPanelController<Azione, ? extends JPanel> controller,
			Posizione2D pos) {

	}

	@Override
	public int getSoliRichiesti() {

		return 50;
	}

	@Override
	public TipoTerreno getTerreno() {

		return TipoTerreno.CORTILE;
	}

	@Override
	public void eseguireAllaMorte() {

	}

	@Override
	public boolean isDead() {

		return this.life <= 0;
	}

}
