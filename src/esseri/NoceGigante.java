/**
 * 
 */
package esseri;

import javax.swing.JPanel;

import controller.AbstractInsertionPanelController;

/**
 * @author Andrea NoceGigante estende Difensivo , come faiRobe fa esattamente la
 *         stessa cosa della Noce , solo che ha una vita molto pi� alta ( per
 *         questo ho utilizzato super.life=Utility,Pianta_vita_alta *2)
 *
 */
public class NoceGigante extends Difensivo {

	private static String img_filePath = "nocegigante.jpe";

	public NoceGigante() {

		super(img_filePath, Utility.PIANTA_VITA_ALTA * 2, 0.0,
				Utility.PIANTA_TEMPO_BASSO, TipoTerreno.CORTILE);

	}

	public void inviaAzione(
			int tempo_trascorso,
			AbstractInsertionPanelController<Azione, ? extends JPanel> controller,
			Posizione2D pos) {

	}

	@Override
	public int getSoliRichiesti() {

		return 100;
	}

	@Override
	public TipoTerreno getTerreno() {

		return TipoTerreno.CORTILE;
	}

	@Override
	public void eseguireAllaMorte() {

	}

	@Override
	public boolean isDead() {

		return this.life <= 0;
	}

}
